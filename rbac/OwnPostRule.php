<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;
use app\models\Post;

class OwnPostRule extends Rule
{
	public $name = 'ownPostRule';
	
	public function execute($user, $item, $params)
	{
		if(isset($_GET['id'])){
				$userId = User::findOne($user);
				$postAuthor = Post::findOne($_GET['id']);
				//הוספנו על מנת לאכוף את החוק של עדכון רק בקטגוריה שבה האחראי קטגוריה שייך אליה
				if(isset($userId) && isset($postAuthor)){
					if($userId->id == $postAuthor->created_by)
						return true;
				}
			}
		return false;
	}
}