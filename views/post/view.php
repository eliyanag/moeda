<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
			
            //'category',
			[  
				'label' => 'Category',
				'value' => function($model){
							return $model->categoryItem->category_name;
					},		
			],
			
            //'author',
			[ 
				'label' => 'Author',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->authorItem->name, 
					['user/view', 'id' => $model->authorItem->id]);
					},		
			],
			
            //'status',
			[ 
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->status_name;
					},		
			],
			
            'created_at',
            'updated_at',
			
            //'created_by',
			[ 
				'label' => 'Created By',
				'value' => function($model){
						return $model->createdByItem->name;
					},		
			],
			
            //'updated_by',
						[ 
				'label' => 'Updated By',
				'value' => function($model){
						return $model->updatedByItem->name;
					},		
			],
        ],
    ]) ?>

</div>
