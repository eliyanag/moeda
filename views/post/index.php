<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body:ntext',
           //'category',
		   [
				'attribute' => 'category',
				'label' => 'Category',
				'value' => function($model){
							return $model->categoryItem->category_name;
					},	
			
			],
		
			 // 'status',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->status_name;
					},		
			],
			
			//'author'
			[
				'attribute' => 'author',
				'label' => 'Author',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->authorItem->name, 
					['user/view', 'id' => $model->authorItem->id]);
				},	
			
                        
                    		
			],
            
           
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
